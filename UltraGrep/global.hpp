/*
	File Name: global.hpp
	Coder: Lexi Shaughnessy
	Student #: 0658874
	Date: November 6, 2015

	This header file uses extern to link global variables (so the program knows of its existence) 
*/
#pragma once
extern vector<vector<string>> results;