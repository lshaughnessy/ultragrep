/*
	File Name: ThreadPool.hpp
	Coder: Lexi Shaughnessy
	Student #: 0658874
	Date: November 6, 2015

	Header file for declaring the variables and functions used to create a thread pool for C++11 threading
	and adding tasks to a queue for threads to retrieve.
*/
#pragma once
#include <queue>
#include <string>
#include <thread>
#include <mutex>
using namespace std;

//C++11 Threads
class ThreadPool {
public:
	bool morePossibleWork;
	string st;
	//c'tor
	ThreadPool()
	{
		morePossibleWork = true;
	}
	queue<string> tasks;
	mutex mTask;
	void perform_task();
	void operator()(int id);
	//mutex mTask;
};//class