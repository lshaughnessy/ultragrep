/*
	File Name: UltraGrep.cpp
	Coder: Lexi Shaughnessy
	Student #: 0658874
	Date: November 6, 2015
	
	This C++11 program recursively searches a files system for text files
	or code files and list all of the lines that contain a given regular expression.
*/

#include "UltraGrep.hpp"
#include "ThreadPool.hpp"
#include <fstream>
#include <iostream>
#include "global.hpp"

/*
	NAME: ReadInputFile()
	PURPOSE: Makes sure whatever is being passed in is a file. 
			 If so, iterate through files and find contents that match regex.
			 Verbose will display names of each file scanned and output whatever the matches were.
	ACCEPTS: two strings 
	RETURNS: boolean
*/
bool UltraGrep::ReadInputFile(string fileName, string st) {
	int fileIdx;
	{lock_guard<mutex> lk(mTask);
		results.push_back(vector<string>());
		fileIdx = results.size() - 1;
	}//mutex
	int lineNumber = 1;
	ifstream inputfile(fileName);
	regex regex(st);
	if (!inputfile) {
		cout << "Could not find file " << fileName;
		return false;
	}//end if
	//verbosity
	if (isVerbose)
	{
		{lock_guard<mutex> lk(mTask);
			cout << "[Scanning] " << fileName << endl;
		}
	}//end if
	string fileLine = "";
	while (getline(inputfile, fileLine))
	{
		int matchCount = 1;
		for (std::sregex_iterator i = std::sregex_iterator(fileLine.begin(), fileLine.end(), regex); i != std::sregex_iterator(); ++i)
		{
			i->str();
			string output = fileName + " Line [" + to_string(lineNumber) + "] Match Number On This Line: " + to_string(matchCount);
			results[fileIdx].push_back(output);
			if (isVerbose)
			{
				{lock_guard<mutex> lk(mTask);
				cout << "[Matched] " << output << " Match: " << i->str() << endl;
				}//mutex
			}// end if
			matchCount++;
		}//end for
		++lineNumber;
	}//end while
	return true;
}//ReadInputFile()

 /*
	 NAME: Scan()
	 PURPOSE: Checks to see what we are searching through is a file or a folder. 
			  If a file, we want to match whatever file we are passing into the regex (regex_search) and add it to the queue
	 ACCEPTS: path to scan, regex of a file extension to look for, ThreadPool object 
	 RETURNS: N/A
 */
void UltraGrep::Scan(path path, regex fileExtension, ThreadPool &tp)
{
	recursive_directory_iterator dir(path), end;
	try {
		for (; dir != end; ++dir)
		{
			//is file
			if (is_regular_file(dir->status()))
			{
				file_count++;
				//matching whatever file we are passing in to the regex
				string fileName = dir->path().string();
				if (regex_search(fileName, fileExtension, regex_constants::match_default))
				{
					tp.tasks.push(fileName);
				}//end if
			}//end if
			 //is folder
			else
			{
				string fileName = dir->path().filename().string();
				folder_count++;
			}//end else
		}//end for
		tp.morePossibleWork = false;
	}//end try
	catch (std::regex_error r) {}
	//return match;
}//Scan()