/*
	File Name: UltraGrep.hpp
	Coder: Lexi Shaughnessy
	Student #: 0658874
	Date: November 6, 2015

	the header file for declaring variables and functions used in the .cpp file of the same name.
*/
#include <Windows.h>
#include <string>
#include <regex>
#include <filesystem>
#include <vector>
#include "ThreadPool.hpp"

using namespace std;
using namespace std::tr2::sys;

class UltraGrep {
public:
	vector<string> fileVec;
	long long file_count = 0;
	long long files_matched = 0;
	long long folder_count = 0;
	bool isRecursive = false;
	bool isVerbose = true;
	string expr;
	string extList;
	mutex mTask;

	bool ReadInputFile(string fileName, string st);
	void Scan(path path, regex fileExtension, ThreadPool &tp);
};//class
