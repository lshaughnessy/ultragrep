/*
	File Name: main.cpp
	Coder: Lexi Shaughnessy
	Student #: 0658874
	Date: November 6, 2015

	the source file for handling command line arguments and starting 
	threading process for the Ultra Grep functionality.
*/
#include "UltraGrep.hpp"
#include "ThreadPool.hpp"
#include <thread>
#include <iostream>
#include "global.hpp"

vector<vector<string>> results;

int main(int argc, char *argv[]) {
	//global
	ThreadPool tp;
	int argIndex = 1;
	int totalMatch = 0;
	int filesWithMatch = 0;
	string filePath;
	vector<thread> threads;
	unsigned nThreads = 4;
	UltraGrep grep1;
	
	//command line 

	//you don't want more than three arguments
	if (argc < 3)
	{
		cout << "Usage: UltraGrep [-v] folder expr [extension-list]*";
		return EXIT_FAILURE;
	}//end if

	//if verbosity (optional argument)
	if (string(argv[argIndex]) == "-v")
	{
		grep1.isVerbose = true;
		++argIndex;
	}//end if

	filePath = string(argv[argIndex]);
	argIndex++;
	tp.st = string(argv[argIndex]);
	argIndex++;

	string ext;
	if (argIndex < argc)
	{
		//it exists!
		ext = string(argv[argIndex]);
	}//end if
	else {
		ext = ".txt";
	}//end else

	//add threads to vector
	for (unsigned i = 0; i < nThreads; i++)
	{	
		threads.push_back(thread(ref(tp), i));
	}//end for
	grep1.Scan(filePath, regex(ext), tp);

	//loop through threads and get them to wait their turn
	for (auto& t : threads)
	{
		t.join();
	}//end for
	cout << "\nGrep Report:\n" << endl;
	for (unsigned i = 0; i < results.size(); i++)
	{
		for (unsigned j = 0; j < results[i].size(); j++)
		{
			cout << results[i][j] << endl;
			totalMatch++;
		}//end for
		if (results[i].size() > 0)
		{
			filesWithMatch++;
		}//end if
	}//end for
	cout << "Files With Matches: " << filesWithMatch << endl;
	cout << "Total Matches: " << totalMatch << endl;
}