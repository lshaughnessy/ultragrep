/*
	File Name: ThreadPool.cpp
	Coder: Lexi Shaughnessy
	Student #: 0658874
	Date: November 6, 2015

	This file holds a thread pool that will have threads check for tasks in a queue and perform them. 
	Otherwise the threads will sleep.
*/

#include "ThreadPool.hpp"
#include "UltraGrep.hpp"
#include <iostream>
#include "global.hpp"
UltraGrep grep;

void ThreadPool::perform_task() {}
/*
	NAME: perform_task()
	PURPOSE: Creates the thread pool for threads tasks in the queue. 
			 Use Critical Sections to prevent race conditions in the code.
	ACCEPTS: int of thread id
	RETURNS: N/A
*/
void ThreadPool::operator()(int id) {

	while (morePossibleWork || tasks.size() > 0)
	{
		string task;
		{lock_guard<mutex> lk(mTask);
			if (tasks.size() > 0)
			{
				task = tasks.front();
				//cout << "ID: " << id << endl;
				tasks.pop();
			}//end if
		}//mutex
		if (task.length() > 0)
		{
			grep.ReadInputFile(task, st);
		}//end if
		Sleep(2000);
	}//end while
}//ThreadPool()
